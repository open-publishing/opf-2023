---
layout:  singlepage.njk
class: "about"
title: Participant Guidelines
permalink: /conduct.html
---

OPF participants at the OPF come from many regions and countries spread across the globe, and in all cases, represent a broad range of experiences and viewpoints. So much of what we are trying to accomplish at each event involves building new relationships, and positive and constructive attitudes serve as the foundation for the work we will do.

While we do not want to dictate behavior or in any way unnecessarily limit expression, we provide the following guidelines as a starting point for collaboration and community building during all the events we co-design and facilitate.

These guidelines are put forward in a proactive stance of trust toward all participants, inviting each person to be their best self, and to help others have most positive meeting experiences.

Participants who fail to honor and follow these guidelines may be asked to leave the meeting at the discretion of the meeting organizers. 

**Guidelines on Respect**

Demonstrate respect for others at all times! :) 

Effective sharing and learning take place when interactions are built on a foundation of mutual respect. In particular, we ask all participants to:

- We are all passionate about our work and have much to say, but it is in listening and understanding how others are thinking and experiencing, and any challenges they are facing, that we become stronger as projects, communities and networks. In particular, it is never ok to interrupt others while they are speaking, and please refrain from side conversations while others are speaking.

- It is always OK to disagree and let others know you think differently on an issue or point. But also remember live meeting time is precious, so please avoid 'hogging the mic'. In moments of strong disagreement, we ask participants to "agree to disagree" so more people can be involved in the discussion and things can keep moving.

**Guidelines on Inclusion**

- Actively strive to include everyone present in all facets of the event. This is an opportunity to broaden networks of trust, skill, collaboration and understanding. Strive to ensure that no one is left out, and speak in language that is accessible to all. 

- Unless you clearly understand everyone involved speaks your language as a first language and are 'experts' then please speak slowly and clearly for those who have English as a second or tenth language, and always make sure to avoid jargon and acronyms, using vocabulary everyone can understand. Where they do need to be used, please define acronyms and technical jargon when introducing them in discussion.

**Guidelines on Creating a Safe Environment**

We are committed to providing a harassment-free conference experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age, religion or nationality. 

**Guidelines on Overall Participation**

- Questions are the currency of collaboration; never suffer in silence when something is unclear. Asking questions is an act of leadership. The event is convened specifically for the benefit of those who need to know more about the topics at hand. Participants should feel free to ask any question at any time; there shall be no such thing as a "stupid question".

- Embrace a spirit of sharing: We believe that everyone present has valuable knowledge and experience to share, and encourage each participant to please contribute their wisdom to the mix. All of us know something and none of us knows everything.

_These Participant Guidelines adapted from the [Aspiration Participant Guidelines](https://facilitation.aspirationtech.org/index.php/Participants:Guidelines)_
