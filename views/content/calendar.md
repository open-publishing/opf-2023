---
layout: calendar.njk
title: Calendar
class: calendar
permalink: /calendar.html
timezoneLabel: Change the timezone

---

<p class="intro">The calendar is updated constantly: dates and times for events may change and new events are added regularly. </p>
<p>Don’t forget that you can still <a class="" href="form.html">propose an event</a> and don’t forget to <a class="" href="participant.html">add your name to the people page!</a></p>