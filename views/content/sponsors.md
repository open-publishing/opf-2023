---
title: "Sponsors"
layout: sponsors.njk
class: "sponsors"
permalink: /sponsors.html
---

# Organizing the Open Publishing Fest 2023

Are you interested to help with the 2023 edition? We are looking for sponsors and wranglers, so let us know if you are interested through the form below:

The 3rd edition of the Open Publishing Festival is happening 13-17 November 2023!

The first two editions saw hundreds of events and thousands of attendees. We're looking forward to learning, celebrating, and doing open publishing with you during the Festival. 

## Are you interested in wrangling?

Let's make sure to keep you in the loop! 
Wrangling means inviting people to submit sessions, helping us decide on the tents for this year, and more. 

## Would you like to sponsor the Festival?

Are you able to talk about sponsoring the Festival? We'd be happy to get in touch and discuss sponsor options.



